package com.example.quizbuster

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.view.setPadding

class QuizActivity : AppCompatActivity() {

    lateinit var progressbarQuiz: ProgressBar
    lateinit var progressbarValue: TextView
    lateinit var tvQuestion: TextView
    lateinit var image: ImageView
    lateinit var tvOptionOne: TextView
    lateinit var tvOptionTwo: TextView
    lateinit var tvOptionThree: TextView
    lateinit var tvOptionFour: TextView
    lateinit var btnSubmitQuiz: Button
    lateinit var tvExplanation: TextView

    private var username: String? = null

    private val questionList = Constants.getQuestions()
    private var currentQuestion: Int = 1
    private val totalNumberOfQuestions: Int = questionList.size
    lateinit var question: Question

    private var isOptionSelected: Boolean = false
    private var numberOfCorrectAnswers: Int = 0
    private var numberOfSkippedAnswers: Int = 0
    lateinit var explanation_text: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz)

        username = intent.getStringExtra(Constants.USERNAME)

        progressbarQuiz = findViewById<ProgressBar>(R.id.progressQuiz)
        progressbarValue = findViewById<TextView>(R.id.tvProgressValue)
        tvQuestion = findViewById<TextView>(R.id.tvQuestion)
        tvOptionOne = findViewById<TextView>(R.id.tvOptionOne)
        tvOptionTwo = findViewById<TextView>(R.id.tvOptionTwo)
        tvOptionThree = findViewById<TextView>(R.id.tvOptionThree)
        tvOptionFour = findViewById<TextView>(R.id.tvOptionFour)
        image = findViewById<ImageView>(R.id.imgQuestionImage)
        btnSubmitQuiz = findViewById<Button>(R.id.btnSubmitQuiz)
        tvExplanation = findViewById<TextView>(R.id.tvExplanation)

        progressbarQuiz.max = totalNumberOfQuestions

        showNextQuestion()

    }
    public fun onOptionSelected(view: View) {
        if(!question.isQuestionPassed) {
            if (!isOptionSelected) {
                val selectedOptionViewId = view.id
                val selectedOptionNumber = getSelectedOptionNumberFromViewId(selectedOptionViewId)
                val correctOptionNumber = questionList.get(currentQuestion - 1).correctOption

                if (correctOptionNumber == selectedOptionNumber) {
                    view.setBackgroundResource(R.drawable.correct_option_border)
                    (view as TextView).setTextColor(getColor(R.color.success_color))
                    numberOfCorrectAnswers++
                    question.selectedOption = selectedOptionNumber

                } else {
                    view.setBackgroundResource(R.drawable.wrong_option_border)
                    (view as TextView).setTextColor(getColor(R.color.danger_color))
                    val correctOptionTextView = findViewById<TextView>(getViewIdFromOptionNumber(correctOptionNumber))
                    correctOptionTextView.setBackgroundResource(R.drawable.correct_option_border)
                    correctOptionTextView.setTextColor(getColor(R.color.success_color))
                    tvExplanation.setText("Explanation: $explanation_text")
                    tvExplanation.setPadding(50)
                    tvExplanation.setBackgroundResource(R.drawable.explanation_border)
                    question.selectedOption = selectedOptionNumber
                }
                isOptionSelected = true
            }
        }
    }
    public fun onSubmitQuiz(view: View) {

        if(!isOptionSelected && question.selectedOption == 0 && !question.isQuestionPassed) {
            numberOfSkippedAnswers++
        }
        currentQuestion++

        if(currentQuestion == totalNumberOfQuestions) {
            btnSubmitQuiz.text = "Submit Quiz"
            showNextQuestion()
        } else if(currentQuestion > totalNumberOfQuestions) {
            Log.i("Result", "Test ended with ${numberOfCorrectAnswers} right and ${numberOfSkippedAnswers} skipped answers")

            val intent = Intent(this, ResultActivity::class.java)
            intent.putExtra(Constants.USERNAME, username)
            intent.putExtra(Constants.TOTAL_NUMBER_OF_QUESTIONS, totalNumberOfQuestions)
            intent.putExtra(Constants.SKIPPED_ANSWERS, numberOfSkippedAnswers)
            intent.putExtra(Constants.CORRECT_ANSWERS, numberOfCorrectAnswers)
            startActivity(intent)
            finish()
        } else {
            showNextQuestion()
        }
    }

    public fun onPreviousQuiz(view: View) {
        if(currentQuestion > 1) {
            currentQuestion--
            progressbarQuiz.progress = currentQuestion
            progressbarValue.text = "${currentQuestion} / ${totalNumberOfQuestions}"

            question = questionList.get(currentQuestion - 1)
            explanation_text = question.explanation
            tvQuestion.text = question.body

            if(! question.hasImage) {
                image.setImageResource(0)
            } else {
                image.setImageResource(question.image)
            }
            tvOptionOne.text = question.options.get(0)
            tvOptionTwo.text = question.options.get(1)
            tvOptionThree.text = question.options.get(2)
            tvOptionFour.text = question.options.get(3)
            setDefaultStatesOfOptions()

            if(question.selectedOption == question.correctOption) {
                tvExplanation.text = ""
                tvExplanation.setPadding(0)
                tvExplanation.setBackgroundResource(R.drawable.no_explanation_border)
                getTvIdfromoptionNumber(question.correctOption).setTextColor(getColor(R.color.success_color))
                getTvIdfromoptionNumber(question.correctOption).setBackgroundResource(R.drawable.correct_option_border)

            }else if (question.selectedOption == 0) {

            } else {
                tvExplanation.setText("Explanation: ${question.explanation}")
                tvExplanation.setPadding(50)
                tvExplanation.setBackgroundResource(R.drawable.explanation_border)
                getTvIdfromoptionNumber(question.selectedOption).setTextColor(getColor(R.color.danger_color))
                getTvIdfromoptionNumber(question.correctOption).setTextColor(getColor(R.color.success_color))
                getTvIdfromoptionNumber(question.correctOption).setBackgroundResource(R.drawable.correct_option_border)
                getTvIdfromoptionNumber(question.selectedOption).setBackgroundResource(R.drawable.wrong_option_border)
            }
        }
    }
    private fun showNextQuestion() {

        progressbarQuiz.progress = currentQuestion
        progressbarValue.text = "${currentQuestion} / ${totalNumberOfQuestions}"
        if (currentQuestion >= 2) {
            question = questionList.get(currentQuestion - 2)
            question.isQuestionPassed = true
        }

        question = questionList.get(currentQuestion - 1)
        if(question.isQuestionPassed) {
            explanation_text = question.explanation
            tvQuestion.text = question.body

            if(! question.hasImage) {
                image.setImageResource(0)
            } else {
                image.setImageResource(question.image)
            }
            tvOptionOne.text = question.options.get(0)
            tvOptionTwo.text = question.options.get(1)
            tvOptionThree.text = question.options.get(2)
            tvOptionFour.text = question.options.get(3)
            setDefaultStatesOfOptions()
            if(question.selectedOption == question.correctOption) {
                tvExplanation.text = ""
                tvExplanation.setPadding(0)
                tvExplanation.setBackgroundResource(R.drawable.no_explanation_border)
                getTvIdfromoptionNumber(question.correctOption).setTextColor(getColor(R.color.success_color))
                getTvIdfromoptionNumber(question.correctOption).setBackgroundResource(R.drawable.correct_option_border)

            } else if (question.selectedOption == 0) {

            } else {
                tvExplanation.setText("Explanation: ${question.explanation}")
                tvExplanation.setPadding(50)
                tvExplanation.setBackgroundResource(R.drawable.explanation_border)
                getTvIdfromoptionNumber(question.selectedOption).setTextColor(getColor(R.color.danger_color))
                getTvIdfromoptionNumber(question.correctOption).setTextColor(getColor(R.color.success_color))
                getTvIdfromoptionNumber(question.correctOption).setBackgroundResource(R.drawable.correct_option_border)
                getTvIdfromoptionNumber(question.selectedOption).setBackgroundResource(R.drawable.wrong_option_border)
            }
        } else {
            explanation_text = question.explanation
            tvQuestion.text = question.body

            if(! question.hasImage) {
                image.setImageResource(0)
            } else {
                image.setImageResource(question.image)
            }
            tvExplanation.text = ""
            tvExplanation.setPadding(0)
            tvExplanation.setBackgroundResource(R.drawable.no_explanation_border)
            tvOptionOne.text = question.options.get(0)
            tvOptionTwo.text = question.options.get(1)
            tvOptionThree.text = question.options.get(2)
            tvOptionFour.text = question.options.get(3)

            isOptionSelected = false
            setDefaultStatesOfOptions()
        }

    }

    private fun getSelectedOptionNumberFromViewId(id: Int): Int {
        when(id) {
            tvOptionOne.id -> return 1
            tvOptionTwo.id -> return 2
            tvOptionThree.id -> return 3
            tvOptionFour.id -> return 4
            else -> return 0
        }
    }

    private fun getViewIdFromOptionNumber(optionNumber: Int): Int {
        when(optionNumber) {
            1 -> return tvOptionOne.id
            2 -> return tvOptionTwo.id
            3 -> return tvOptionThree.id
            4 -> return tvOptionFour.id
            else -> return 0
        }
    }

    private fun getTvIdfromoptionNumber(optionNumber: Int): TextView {
        when(optionNumber) {
            1 -> return tvOptionOne
            2 -> return tvOptionTwo
            3 -> return tvOptionThree
            4 -> return tvOptionFour
            else -> return tvOptionOne
        }
    }

    private fun setDefaultStatesOfOptions() {
        tvOptionOne.setBackgroundResource(R.drawable.default_border)
        tvOptionTwo.setBackgroundResource(R.drawable.default_border)
        tvOptionThree.setBackgroundResource(R.drawable.default_border)
        tvOptionFour.setBackgroundResource(R.drawable.default_border)

        tvOptionOne.setTextColor(getColor(R.color.primary_text_color))
        tvOptionTwo.setTextColor(getColor(R.color.primary_text_color))
        tvOptionThree.setTextColor(getColor(R.color.primary_text_color))
        tvOptionFour.setTextColor(getColor(R.color.primary_text_color))
    }
}